#Knoob Recursive Bzip Util#

##Description##
A simple tool written in C++11 to recursively traverse a directory and bzip all files contained.
Intended use is for SRCDS FastDL servers.

##License##
Simple BSD - see LICENSE.md

##Requirements##
A working copy of the bzip2 util.
A Unix server environment. (Untested on Windows, but shouldn't be too hard to port in cygwin or equivilent)
G++ with C++11.

##Thanks##
Thanks to the tinydir guys, made this much easier!
https://github.com/cxong/tinydir

